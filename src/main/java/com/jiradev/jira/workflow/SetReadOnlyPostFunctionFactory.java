package com.jiradev.jira.workflow;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.*;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class SetReadOnlyPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory{

    private WorkflowManager workflowManager;
    private CustomFieldManager customFieldManager;
    private final String CUSTOMFIELDS = "customFields";
    private final String SELECTEDFIELD = "selectedField";
    private final String VALUE = "value";


    public SetReadOnlyPostFunctionFactory(WorkflowManager workflowManager, CustomFieldManager customFieldManager){

        this.workflowManager=workflowManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object>velocityParams){

        Map<String, String[]>myParams=ActionContext.getParameters();

                //the default message
        velocityParams.put(CUSTOMFIELDS,customFieldManager.getCustomFieldObjects());

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object>velocityParams,AbstractDescriptor descriptor){

        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
        FunctionDescriptor functionDescriptor=(FunctionDescriptor)descriptor;
        String selectedField=(String)functionDescriptor.getArgs().get(SELECTEDFIELD);
        String value=(String)functionDescriptor.getArgs().get(VALUE);
        velocityParams.put(SELECTEDFIELD, selectedField);
        velocityParams.put(VALUE, value);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object>velocityParams,AbstractDescriptor descriptor){
        if(!(descriptor instanceof FunctionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor=(FunctionDescriptor)descriptor;

        String selectedField=(String)functionDescriptor.getArgs().get(SELECTEDFIELD);
        String value=(String)functionDescriptor.getArgs().get(VALUE);

        if(selectedField==null){
            selectedField="No custom field selected";
        }
        else{
            selectedField = customFieldManager.getCustomFieldObject(selectedField).getName();
        }

        velocityParams.put(SELECTEDFIELD,selectedField);
        velocityParams.put(VALUE, value);
    }


    public Map<String,?>getDescriptorParams(Map<String, Object>formParams){
        Map params=new HashMap();

        // Process The map
        String selectedField=extractSingleParam(formParams,SELECTEDFIELD);
        String value= extractSingleParam(formParams,VALUE);
        params.put(SELECTEDFIELD,selectedField);
        params.put(VALUE,value);
        return params;
    }

}