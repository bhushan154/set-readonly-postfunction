package com.jiradev.jira.workflow;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class SetReadOnlyPostFunction extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(SetReadOnlyPostFunction.class);
    public static final String SELECTEDFIELD="selectedField";
    private final String VALUE = "value";
    private CustomFieldManager customFieldManager;

    public SetReadOnlyPostFunction(CustomFieldManager customFieldManager){
        this.customFieldManager = customFieldManager;
    }

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        String selectedField = (String)args.get(SELECTEDFIELD);
        String value = (String)args.get(VALUE);
        CustomField field = null;

        if(null != selectedField) {
            field = customFieldManager.getCustomFieldObject(selectedField);
        }

        issue.setCustomFieldValue(field, value);
    }
}