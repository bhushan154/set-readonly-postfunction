Tutorial available at http://blog.jiradev.com/2014/09/can-we-have-tutorial-on-how-to-set.html

Got a new request on www.jiradev.com for a tutorial to set a readonly custom field through post function. So here it is.

Start by creating a JIRA plugin skeleton using the command atlas-create-jira-plugin. For more information, take a look at the tutorial at http://jiradev.com/plugin-skeleton.html

```
Group ID: com.jiradev
Artifact ID: setreadonly
Version: Press Enter
Package: Press Enter

```

From the new directory named setreadonly, create a new module for workflow post-function using the command "atlas-create-jira-plugin-module". Select option 33 for post-function. 

```
Classname: SetReadOnlyPostFunction
Package: com.jiradev.jira.workflow
Show advanced setup: N
Add another plugin module: N
```

You now have to update the code on the following files.

com.jiradev.jira.workflow.SetReadOnlyPostFunctionFactory.java

```
package com.jiradev.jira.workflow;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.*;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class SetReadOnlyPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory{

    private WorkflowManager workflowManager;
    private CustomFieldManager customFieldManager;
    private final String CUSTOMFIELDS = "customFields";
    private final String SELECTEDFIELD = "selectedField";
    private final String VALUE = "value";


    public SetReadOnlyPostFunctionFactory(WorkflowManager workflowManager, CustomFieldManager customFieldManager){

        this.workflowManager=workflowManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object>velocityParams){

        Map<String, String[]>myParams=ActionContext.getParameters();

                //the default message
        velocityParams.put(CUSTOMFIELDS,customFieldManager.getCustomFieldObjects());

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object>velocityParams,AbstractDescriptor descriptor){

        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
        FunctionDescriptor functionDescriptor=(FunctionDescriptor)descriptor;
        String selectedField=(String)functionDescriptor.getArgs().get(SELECTEDFIELD);
        String value=(String)functionDescriptor.getArgs().get(VALUE);
        velocityParams.put(SELECTEDFIELD, selectedField);
        velocityParams.put(VALUE, value);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object>velocityParams,AbstractDescriptor descriptor){
        if(!(descriptor instanceof FunctionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor=(FunctionDescriptor)descriptor;

        String selectedField=(String)functionDescriptor.getArgs().get(SELECTEDFIELD);
        String value=(String)functionDescriptor.getArgs().get(VALUE);

        if(selectedField==null){
            selectedField="No custom field selected";
        }
        else{
            selectedField = customFieldManager.getCustomFieldObject(selectedField).getName();
        }

        velocityParams.put(SELECTEDFIELD,selectedField);
        velocityParams.put(VALUE, value);
    }


    public Map<String,?>getDescriptorParams(Map<String, Object>formParams){
        Map params=new HashMap();

        // Process The map
        String selectedField=extractSingleParam(formParams,SELECTEDFIELD);
        String value= extractSingleParam(formParams,VALUE);
        params.put(SELECTEDFIELD,selectedField);
        params.put(VALUE,value);
        return params;
    }

}
```

com.jiradev.jira.workflow.SetReadOnlyPostFunction

```
package com.jiradev.jira.workflow;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class SetReadOnlyPostFunction extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(SetReadOnlyPostFunction.class);
    public static final String SELECTEDFIELD="selectedField";
    private final String VALUE = "value";
    private CustomFieldManager customFieldManager;

    public SetReadOnlyPostFunction(CustomFieldManager customFieldManager){
        this.customFieldManager = customFieldManager;
    }

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        String selectedField = (String)args.get(SELECTEDFIELD);
        String value = (String)args.get(VALUE);
        CustomField field = null;

        if(null != selectedField) {
            field = customFieldManager.getCustomFieldObject(selectedField);
        }

        issue.setCustomFieldValue(field, value);
    }
}
```

templates/postfunctions/set-read-only-post-function-input.vm

```
<tr>
    <td class="fieldLabelArea">
        Select custom field
    </td>
    <td nowrap>
        <select name="selectedField" id="selectedField">
            #foreach($field in $customFields)
                <option value="$field.getId()"
                    #if($SELECTEDFIELD == $field.getId()) selected#end
                >
                    $field.getName()
                </option>
            #end
        </select>
    </td>
</tr>
<tr>
    <td class="fieldLabelArea">
        Enter value
    </td>
    <td nowrap>
        <input name="value" id="value" #if($value)value="$value"#end />
    </td>
</tr>
```

templates/postfunction/set-read-only-post-function.vm

```
Sets the value of the custom field <b>$selectedField</b> to <b>$value</b>.
``` 

To test the plugin, create a read only custom field from the JIRA Administration. For more information on how to create a custom field, refer to the documentation available at https://confluence.atlassian.com/display/JIRA/Adding+a+Custom+Field

# Create a workflow and add a new post-function. You will see a new "Set Read Only Post Function".
# Select a custom field and enter a value you would like to set.
# Create a workflow scheme, associate it to the new workflow, create a new project that uses the workflow and check if the post-function works.

For more tutorials, visit www.jiradev.com
